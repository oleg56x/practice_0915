package com.farhad.task2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

/**
 * Created by FARHAD on 23.09.2015.
 */
public class SecondActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_layout);
        ImageView img = (ImageView) findViewById(R.id.img2);

        int position = getIntent().getIntExtra("position", -1);
        if(position != -1)  {
            Picasso.with(SecondActivity.this)
                    .load(MyAdapter.urls[position])
                    .error(R.drawable.err)
                    .placeholder(R.drawable.adele)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(img);
        }   else {
            Picasso.with(SecondActivity.this)
                    .load(R.drawable.err)
                    .placeholder(R.drawable.adele)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(img);
        }


    }


}
