package com.farhad.task4;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import java.sql.Time;
import java.util.Date;


public class MainActivity extends AppCompatActivity {
    // Идентификатор уведомления
    private static final int NOTIFY_ID = 101;
    public static final String TAG_MESSAGE = "tag11";
    AlarmManager am;
    Time stamp;
    NotificationManager nm;
    Intent intent;
    Wifi n = new Wifi();
    String wifiString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


               findViewById(R.id.wifibut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText ed = new EditText(MainActivity.this);
                ed.setHint("wifi name");
                if (wifiString != null)
                    ed.setText(wifiString);
                new AlertDialog.Builder(MainActivity.this)
                        .setView(ed)
                        .setPositiveButton("start monitoring", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                startMonitoring(String.valueOf(ed.getText()));
                            }
                        })
                        .setNeutralButton("stop monitoring", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                stopMonitoring(String.valueOf(ed.getText()));
                            }
                        }).create().show();
            }
        });

        findViewById(R.id.onModeB).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainActivity.this).create().show();
            }
        });

        findViewById(R.id.timenot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Context context = getApplicationContext();
                        Notification.Builder builder = new Notification.Builder(context)
                                .setContentTitle("time")
                                .setContentText("time2")
                                .setTicker("Уведомление!!!").setWhen(view.getBaseline())
                                        // .setContentIntent(pendingIntent)
                                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(true)
                                .setSmallIcon(R.drawable.audio)
                                .setProgress(100, 50, false);

                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(NOTIFY_ID, builder.build());
                    }
                }, new Date().getHours(), new Date().getMinutes(), true).show();
            }
        });

        findViewById(R.id.timenot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        Context context = getApplicationContext();
                        Notification.Builder builder = new Notification.Builder(context)
                                .setContentTitle("time")
                                .setContentText("time2")
                                .setTicker("Уведомление!!!").setWhen(view.getBaseline())
                                        // .setContentIntent(pendingIntent)
                                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(true)
                                .setSmallIcon(R.drawable.audio)
                                .setProgress(100, 50, false);

                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(NOTIFY_ID, builder.build());
                    }
                }, new Date().getHours(), new Date().getMinutes(), true).show();
            }
        });


    }



    /*private void setTimeNotification(int hourOfDay, int minute) {

        Context context = getApplicationContext();
        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle("time")
                .setContentText("time2")
                .setTicker("Уведомление!!!").setWhen()
                        // .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(true)
                .setSmallIcon(R.drawable.audio)
                .setProgress(100, 50, false);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFY_ID, builder.build());
    }*/



/*
    public void onClick1 (View view){

        Context context = getApplicationContext();

        Time time = null;
        TimePicker timePicker = (TimePicker)findViewById(R.id.timep);
        time.setTime(timePicker.getDrawingTime());
        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle("По нажатию кнопки")
                .setContentText("ничего не происходит")
                .setTicker("Уведомление!!!").setWhen(timePicker.getBaseline())
                        // .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(true)
                .setSmallIcon(R.drawable.audio)
                .setProgress(100, 50, false);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFY_ID, builder.build());
    }
*/


    public void onClick(View view) {
        Context context = getApplicationContext();
       /* Intent notificationIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://developer.alexanderklimov.ru/android/"));
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);*/

        Notification.Builder builder = new Notification.Builder(context)
                .setContentTitle("По нажатию кнопки")
                .setContentText("ничего не происходит")
                .setTicker("Уведомление!!!").setWhen(System.currentTimeMillis())
                        // .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL).setAutoCancel(true)
                .setSmallIcon(R.drawable.audio)
                .setProgress(100, 50, false);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFY_ID, builder.build());
    }
/*
    public void onClick2(View view) {
        final EditText ed = (EditText) findViewById(R.id.wifi);

        if (ed.getText().toString() != null)
            new AlertDialog.Builder(MainActivity.this)
                    .setView(ed)
                    .setPositiveButton("start monitoring", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startMonitoring(String.valueOf(ed.getText()));
                        }
                    })
                    .setNeutralButton("stop monitoring", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog,  int which) {
                            stopMonitoring(String.valueOf(ed.getText()));
                        }
                    }).create().show();
            }
*/


    private void stopMonitoring(String name) {
        if (n != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(n);
        }
    }

    private void startMonitoring(String name) {
        if (name != null && !name.equals("")) {
            LocalBroadcastManager.getInstance(this).registerReceiver(n, new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION));
            Intent intent = new Intent(WifiManager.NETWORK_STATE_CHANGED_ACTION);
            intent.putExtra(TAG_MESSAGE, name);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }
    }

}
