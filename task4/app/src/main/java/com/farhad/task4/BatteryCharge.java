package com.farhad.task4;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.BatteryManager;
import android.support.v4.app.NotificationCompat;
import android.content.BroadcastReceiver;
import android.content.Context;


/**
 * Created by FARHAD on 29.09.2015.
 */
public class BatteryCharge  extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                    new Intent(context, MainActivity.class), 0);

            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            if (level != -1 && scale != -1) {
                float value = level / (float) scale;
                if (value < 30) {

                    NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                    builder.mContentText = "on wifi";
                    builder.mContentTitle = "notification on wifi connection";
                    builder.setSmallIcon(android.support.v7.appcompat.R.drawable.abc_btn_radio_material);
                    Notification notification = builder.build();
                    NotificationManager systemService = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    systemService.notify(null, 2, notification);
                    notification.contentIntent = pendingIntent;


                }
            }
        }
    }
