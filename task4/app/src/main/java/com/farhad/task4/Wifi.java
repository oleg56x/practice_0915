package com.farhad.task4;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.NotificationCompat;

/**
 * Created by FARHAD on 28.09.2015.
 */
public class Wifi extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        String name = intent.getStringExtra(MainActivity.TAG_MESSAGE);
        if (name != null && getWifiName(context).contains(name)) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.mContentText = "on wifi";
            builder.mContentTitle = "notification on wifi connection";
            builder.setSmallIcon(android.support.v7.appcompat.R.drawable.abc_btn_radio_material);
            Notification notification = builder.build();
            NotificationManager systemService = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            systemService.notify(null, 2, notification);
        }

    }

    private String getWifiName(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return wifiInfo.getSSID();
    }

}