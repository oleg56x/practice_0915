package com.farhad.mmmap;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by FARHAD
 */
public class CustLoader extends AsyncTaskLoader<ArrayList<Markers>> {

    LatLng me;
    private static final String REQUEST =
            "https://geocode-maps.yandex.ru/1.x/?geocode=%s,%s&kind=locality&rspn=0&format=json&sco=latlong";

    public CustLoader(Context context, LatLng me) {
        super(context);
        this.me = me;
    }

    @Override
    public ArrayList<Markers> loadInBackground() {

        String url = String.format(REQUEST, me.latitude, me.longitude);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
           Response response = client.newCall(request).execute();
            String string = response.body().string();

            return XParce.parse(string);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }



}

