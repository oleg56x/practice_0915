package com.farhad.mmmap;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by FARHAD on 24.09.2015.
 */
public class XParce {

    private static final String JSON_RESPONSE = "response";
    private static final String JSON_GEO_OBJECT_COLLECTION = "GeoObjectCollection";
    private static final String JSON_FEATURE_MEMBER = "featureMember";
    private static final String JSON_GEO_OBJECT = "GeoObject";
    private static final String JSON_NAME = "name";
    private static final String JSON_Markers = "Point";
    private static final String JSON_POS = "pos";

    public static ArrayList<Markers> parse(String json) {
        ArrayList<Markers> places = new ArrayList<>();

        JSONObject jsonObject;
        JSONArray placesJson;
        try {
            jsonObject = new JSONObject(json);
            placesJson = jsonObject.getJSONObject(JSON_RESPONSE).getJSONObject(JSON_GEO_OBJECT_COLLECTION)
                    .getJSONArray(JSON_FEATURE_MEMBER);
        } catch (Exception e) {
            Log.e("ParserUtils", "error", e);
            return places;
        }

        for (int i = 0; i < placesJson.length(); i++) {

            try {
                JSONObject geoObject = placesJson.getJSONObject(i).getJSONObject(JSON_GEO_OBJECT);
                JSONObject Markers = geoObject.getJSONObject(JSON_Markers);
                String[] coordinates = Markers.getString(JSON_POS).split(" ");
                places.add(
                        new Markers(new LatLng(
                                Double.parseDouble(coordinates[1]),
                                Double.parseDouble(coordinates[0])), geoObject.getString(JSON_NAME)));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return places;
    }

    public static List<LatLng> asArray(List<Markers> data) {
        List<LatLng> latLngs = new ArrayList<>(data.size());
        for (Markers p : data) {
            latLngs.add(p.getPosition());
        }
        return latLngs;
    }
}


