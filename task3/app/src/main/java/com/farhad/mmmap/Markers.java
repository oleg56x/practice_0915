package com.farhad.mmmap;

import android.content.ContentValues;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by FARHAD
 */
public class Markers {

    LatLng pos;
    int id;
    String name;
    String description;

    public Markers(LatLng pos, String name){
        this.pos = pos;
        this.name = name;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMarkerName(String markerName) {
        this.name = name;
    }

    public void setPosition(LatLng position) {
        this.pos = pos;
    }

    public LatLng getPosition() {
        return pos;
    }

    public String getMarkerName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public MarkerOptions asMarker() {
        MarkerOptions markerOptions = new MarkerOptions().position(pos).title(name);
        if (description != null)
            markerOptions.snippet(description);
        return markerOptions;
    }

    public ContentValues asContentValues() {

        ContentValues cv = new ContentValues();
        cv.put(MarkerProvider.POINT_NAME, getMarkerName());
        cv.put(MarkerProvider.POINT_LON, getPosition().longitude);
        cv.put(MarkerProvider.POINT_LAT, getPosition().latitude);
        cv.put(MarkerProvider.POINT_DESCRIPTION, getDescription());
        return cv;
    }
}
