package com.farhad.mmmap;

import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;

import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback,
        LoaderManager.LoaderCallbacks<ArrayList<Markers>>, RoutingListener {

    private static final int LOADER_NEAREST_POINTS = 1;


    GoogleApiClient mGoogleApiClient;

    LatLng currentLatLng;

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setUpMapIfNeeded();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void addClosestPlaces(LatLng lantng) {
        Bundle bndl = new Bundle();
        bndl.putParcelable("latng", lantng);
        getLoaderManager().initLoader(LOADER_NEAREST_POINTS, bndl, this);
    }


    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }


    @Override
    public Loader<ArrayList<Markers>> onCreateLoader(int id, Bundle args) {
        CustLoader getter = new CustLoader(this, (LatLng) args.getParcelable("latng"));
        getter.forceLoad();
        return getter;
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Markers>> loader, ArrayList<Markers> data) {
        for (Markers p : data) {
            mMap.addMarker(p.asMarker());
        }
        /*   Routing routing = new Routing.Builder()
                    .travelMode(Routing.TravelMode.WALKING)
                    .withListener(this)
                    .waypoints(XParce.asArray(data))
                    .build();
            routing.execute();*/

    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Markers>> arrayListLoader) {

    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(Bundle bundle) {
//        Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        //     if (mCurrentLocation != null) {

        double currentLatitude = 55.7955015;
        // mCurrentLocation.getLatitude();
        double currentLongitude = 49.073303;
        //mCurrentLocation.getLongitude();


        currentLatLng = new LatLng(currentLatitude, currentLongitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 13));
        myLock();
        addClosestPlaces(new LatLng(currentLatitude, currentLongitude));
        //      }
    }

    private void myLock() {
        mMap.addMarker(new MarkerOptions()
                .position(currentLatLng)
                .title("mypos")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        buildGoogleApiClient();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            Log.e("Main", "Connection Failed");
        }

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(final Marker marker) {

                final EditText ed = new EditText(MapsActivity.this);
                ed.setText(marker.getSnippet());
                ed.setHint("Input description here");
                new AlertDialog.Builder(MapsActivity.this)
                        .setView(ed)
                        .setCancelable(true)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Markers markers = new Markers(marker.getPosition(), marker.getTitle());
                                markers.setDescription(String.valueOf(ed.getText()));
                                getContentResolver().update(
                                        MarkerProvider.CONTACT_CONTENT_URI,
                                        markers.asContentValues(),
                                        MarkerProvider.POINT_LAT + "=? and "
                                                + MarkerProvider.POINT_LON + "=? ",
                                        new String[]{
                                                String.valueOf(marker.getPosition().latitude),
                                                String.valueOf(marker.getPosition().longitude),
                                        }
                                );
                                marker.setSnippet(String.valueOf(ed.getText()));
                            }
                        })
                        .setNeutralButton("Remove", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getContentResolver().delete(
                                        MarkerProvider.CONTACT_CONTENT_URI,
                                        MarkerProvider.POINT_LAT + "=? and "
                                                + MarkerProvider.POINT_LON + "=? ",
                                        new String[]{
                                                String.valueOf(marker.getPosition().latitude),
                                                String.valueOf(marker.getPosition().longitude),
                                        }
                                );
                                updateMap();
                            }
                        })
                        .create().show();
            }
        });
        lookupDatabase();
    }


    private void updateMap() {
        mMap.clear();
        myLock();
        List<LatLng> latLngs = lookupDatabase();
        createWay(latLngs);
    }

    private List<LatLng> lookupDatabase() {
        List<LatLng> latLngs = new ArrayList<>(10);
        Cursor cursor = getContentResolver().query(MarkerProvider.CONTACT_CONTENT_URI, null, null, null, null);
        if (cursor.moveToFirst())
            do {
                LatLng position = new LatLng(
                        cursor.getDouble(2),
                        cursor.getDouble(3)
                );
                mMap.addMarker(new MarkerOptions()
                        .title(cursor.getString(1))
                        .position(position)
                        .snippet(cursor.getString(4)));
                latLngs.add(position);
            }
            while (cursor.moveToNext());

        return latLngs;

    }

    private void createWay(List<LatLng> waypoints) {
        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.WALKING)
                .withListener(this)
                .waypoints(waypoints)
                .build();
        routing.execute();

    }


    @Override
    public void onRoutingFailure() {

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(PolylineOptions polylineOptions, Route route) {
        PolylineOptions polyoptions = new PolylineOptions();
        polyoptions.color(Color.BLUE);
        polyoptions.width(11);
        polyoptions.addAll(polylineOptions.getPoints());
        mMap.addPolyline(polyoptions);
    }

    @Override
    public void onRoutingCancelled() {

    }
}
