package com.farhad.mmmap;

import android.content.AsyncTaskLoader;
import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by FARHAD
 */
public class CustLoader extends AsyncTaskLoader<ArrayList<Markers>> {

    LatLng me;
    private static final String REQUEST =
            "https://geocode-maps.yandex.ru/1.x/?geocode=%s,%s&kind=locality&rspn=0&format=json&sco=latlong";

    public CustLoader(Context context, LatLng me) {
        super(context);
        this.me = me;
    }

    @Override
    public ArrayList<Markers> loadInBackground() {

        String url = String.format(REQUEST, me.latitude, me.longitude);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        try {
           Response response = client.newCall(request).execute();
            String string = response.body().string();
        //    String string = "{\"response\":{\"GeoObjectCollection\":{\"metaDataProperty\":{\"GeocoderResponseMetaData\":{\"request\":\"59.786438,49.122458\",\"found\":\"3\",\"results\":\"10\",\"boundedBy\":{\"Envelope\":{\"lowerCorner\":\"48.872457 59.535495\",\"upperCorner\":\"49.372459 60.035499\"}},\"Point\":{\"pos\":\"49.122458 59.786438\"},\"kind\":\"locality\"}},\"featureMember\":[{\"GeoObject\":{\"metaDataProperty\":{\"GeocoderMetaData\":{\"kind\":\"locality\",\"text\":\"Россия, Кировская область, Мурашинский район, поселок Тылай\",\"precision\":\"other\",\"AddressDetails\":{\"Country\":{\"AddressLine\":\"Кировская область, Мурашинский район, поселок Тылай\",\"CountryNameCode\":\"RU\",\"CountryName\":\"Россия\",\"AdministrativeArea\":{\"AdministrativeAreaName\":\"Кировская область\",\"SubAdministrativeArea\":{\"SubAdministrativeAreaName\":\"Мурашинский район\",\"Locality\":{\"LocalityName\":\"поселок Тылай\"}}}}}}},\"description\":\"Мурашинский район, Кировская область, Россия\",\"name\":\"поселок Тылай\",\"boundedBy\":{\"Envelope\":{\"lowerCorner\":\"48.869663 59.600041\",\"upperCorner\":\"48.88188 59.609688\"}},\"Point\":{\"pos\":\"48.875520 59.604526\"}}},{\"GeoObject\":{\"metaDataProperty\":{\"GeocoderMetaData\":{\"kind\":\"locality\",\"text\":\"Россия, Кировская область, Мурашинский район, урочище Русский Кузюг\",\"precision\":\"other\",\"AddressDetails\":{\"Country\":{\"AddressLine\":\"Кировская область, Мурашинский район, урочище Русский Кузюг\",\"CountryNameCode\":\"RU\",\"CountryName\":\"Россия\",\"AdministrativeArea\":{\"AdministrativeAreaName\":\"Кировская область\",\"SubAdministrativeArea\":{\"SubAdministrativeAreaName\":\"Мурашинский район\",\"Locality\":{\"LocalityName\":\"урочище Русский Кузюг\"}}}}}}},\"description\":\"Мурашинский район, Кировская область, Россия\",\"name\":\"урочище Русский Кузюг\",\"boundedBy\":{\"Envelope\":{\"lowerCorner\":\"48.887405 59.538693\",\"upperCorner\":\"48.890639 59.541435\"}},\"Point\":{\"pos\":\"48.888995 59.540066\"}}},{\"GeoObject\":{\"metaDataProperty\":{\"GeocoderMetaData\":{\"kind\":\"locality\",\"text\":\"Россия, Республика Коми, Прилузский район, поселок Бедьвож\",\"precision\":\"other\",\"AddressDetails\":{\"Country\":{\"AddressLine\":\"Республика Коми, Прилузский район, поселок Бедьвож\",\"CountryNameCode\":\"RU\",\"CountryName\":\"Россия\",\"AdministrativeArea\":{\"AdministrativeAreaName\":\"Республика Коми\",\"SubAdministrativeArea\":{\"SubAdministrativeAreaName\":\"Прилузский район\",\"Locality\":{\"LocalityName\":\"поселок Бедьвож\"}}}}}}},\"description\":\"Прилузский район, Республика Коми, Россия\",\"name\":\"поселок Бедьвож\",\"boundedBy\":{\"Envelope\":{\"lowerCorner\":\"49.355274 60.029831\",\"upperCorner\":\"49.367653 60.034596\"}},\"Point\":{\"pos\":\"49.361814 60.032250\"}}}]}}}";

            return XParce.parse(string);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }



}

