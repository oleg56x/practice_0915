package com.farhad.mmmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by FARHAD
 */
public class Markers {

    LatLng pos;
    String id;

    public Markers(LatLng pos, String id){
        this.pos = pos;
        this.id = id;
    }

    public LatLng getPos(){
        return pos;
    }

    public String getId(){
        return id;

    }

    public MarkerOptions asMarker() {
        return new MarkerOptions().position(pos).title(id);
    }

    public LatLng getPosition() {
        return pos;
    }

}
